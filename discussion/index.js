// Javascript Operators

// Arithmetic Operators

let x = 1397;
let y = 7831;

let sum = x + y;
console.log("Result of addition operator: " + sum);

let difference = x - y;
console.log("Result of subtraction operator: " + difference);

let product = x * y;
console.log("Result of multiplication operator: " + product);

let quotient = x / y;
console.log("Result of division operator:" + quotient);

let remainder = y % x;
console.log("Result of modulo operator: " + remainder);


// Assaignment Operators (=) - assigns the value of the right operand to a variable
let assignmentNumber = 8;

// Addition assignment operator (+=)
assignmentNumber = assignmentNumber + 2;
console.log("Result off addition assignment operator: " + assignmentNumber);
//Shorthand
assignmentNumber += 2;
console.log(' Result of addition assignment opertion: ' + assignmentNumber);

//Subtraction/Multiplication/Division Assignment Operator (-=, *=, /=)
assignmentNumber -= 2;
console.log("Result of subtraction assignment operator: " + assignmentNumber);
assignmentNumber *= 2;
console.log("Result of multiplication assignment operator: " + assignmentNumber);
assignmentNumber /= 2;
console.log("Result of Division assignment operator: " + assignmentNumber);


// Multiple Operators and parentheses
let mdas = 1 + 2 - 3 * 4 / 5;
console.log("Result of mdas operation: " + mdas);

//Using parenthesis
let pemdas = 1 + (2 - 3) * (4 / 5);
console.log("Result of pemdas operation: " + pemdas);
pemdas = (1 + (2 - 3)) * (4 / 5);
console.log("Result of pemdas operation: " + pemdas);

// Increment and Decrement (++, __)
let z = 1;
// Pre-increment
let increment = ++z;
console.log("Result of pre-increment: " + increment);
console.log("Result of pre-increment: " + z);
//post-increment
increment = z++;
console.log("Result of pre-increment: " + increment);
console.log("Result of pre-increment: " + z);
let decrement = --z;
console.log("Result of pre-decrement: " + decrement);
console.log("Result of pre-decrement: " + z);
decrement = z--;
console.log("Result of pre-decrement: " + decrement);
console.log("Result of pre-decrement: " + z);

// Type Coercion
let numA = '10';
let = numB = 12;
let coercion = numA + numB;
console.log(coercion);
console.log(typeof coercion);
let numC = true + 1;
console.log(numC);

// Comparison Operators
let juan = 'juan';

// Equality operator (==)
console.log(1 == 1);
console.log(1 == 2);
console.log(1 == '1');
console.log(0 == false);
console.log('juan' == juan);

// Inequality operator (!=)
console.log(1 != 1);
console.log(1 != 2);
console.log(1 != '1');
console.log(0 != false);
console.log('juan' == juan);

//Strict Equality Operator (===)
console.log(1 === 1);
console.log(1 == 2);
console.log(1 === '1');
console.log(0 === false);
console.log('juan' === juan);

//Strict Inequality operator (!==)
console.log(1 !== 1);
console.log(1 !== 2);
console.log(1 !== '1');
console.log(0 !== false);
console.log('juan' !== juan);

//Relational Operators 
let a = 50;
let b = 65;

//Greater than (>)
let isGreaterThan = a > b;
//Less htan (<)
let isLessThan = a < b;
// Greater than or equal
let isGTorEqual = a >= b;
// Less than or equal
let isLTorEqual = a <= b;

console.log(isGreaterThan);
console.log(isLessThan);
console.log(isGTorEqual);
console.log(isLTorEqual);

//Logical and Operator (&&)
//Returns true if all operators are true
let allRequirementsMet = isLegalAge && isRegistered;
console.log("Result of logical AND operator: " + allRequirementsMet);
//Logical 'OR' Operator (||)
// Returns TRUE if one of the operands are true
let someRequirementsMet = isLegalAge || isRegistered;
console.log("Result of logical Operator 'OR: " + someRequirementsMet);
// Logical 'NOt' Operator (!)
// returns the Opposite Value
let someRequirementsNotMet = !isRegistered;
console.log("Result of Logical 'NOT' Operator: " + someRequirementsNotMet)